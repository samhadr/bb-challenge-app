var express = require('express');
var app = express();

app.set('view engine', 'ejs');

app.get('/', function(req, res) {
	res.render('default', {
		title: 'Home',
		classname: 'home',
		users: ['Sam', 'Lisa', 'Ileana']
	});
});

app.get('/about', function(req, res) {
	res.render('default', {
		title: 'About Us',
		classname: 'about'
	});
});

app.get('*', function(req, res) {
	res.send('Bad route');
});

var server = app.listen(80, function() {
	console.log('Listening on port 80')
});